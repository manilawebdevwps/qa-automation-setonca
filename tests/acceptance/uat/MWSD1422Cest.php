<?php
namespace uat;
use \WebGuy;

class MWSD1422Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckLiveChat(WebGuy $I)
    {
        $I->amOnPage('/');
        $I->wantTo('See LivePerson links in the Live Chat header');
        $I->canSeeLink('Live Chat');
        $I->click('Live Chat');
        $I->wait(5);
        $I->switchToWindow('Live Chat');
        $I->seeInCurrentUrl('/hc/29107782/?cmd=file&file=visitorWantsToChat&site=29107782&SESSIONVAR!skill=Seton%20CA');
    }

}