<?php
namespace uat;
use \WebGuy;

class MWSD1488Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkGuestCheckoutCheckboxInBilling(WebGuy $I) {
        $I->amOnPage('customer/account/login/');
        $I->fillField('#email','alfredo_regala@bradycorp.com');
        $I->fillField('#pass','p@ssw0rd');
        $I->click('#send2');
        $I->amOnPage('labels-and-decals/safety/safety/electrical/machine-warning-labels-91620.html');
        $I->click('#btnaddtocart');
        sleep(10);
        $I->click('img[alt="Cart"]');
        $I->click('button[title="BEGIN SECURE CHECKOUT"]');
        $I->click('button[title="Continue"]');
        sleep(10);
        //$I->waitForUserInput();
        $I->fillField('ship_to_deliverypoint','Test only');
        sleep(10);
        $I->click('/html/body/div/div[6]/div/div/div/div/ol/li[3]/div[2]/div/div/form/div[3]/button');
        sleep(10);
        $I->click('/html/body/div/div[6]/div/div/div/div/ol/li[4]/div[2]/form/fieldset/dl/dl/div/div[2]/dt[2]/input');
        sleep(10);
        $I->click('/html/body/div/div[6]/div/div/div/div/ol/li[4]/div[2]/div[2]/button');
        sleep(10);
        //$I->seeElement('input[type="checkbox"]');
    }

}