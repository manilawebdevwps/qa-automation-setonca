<?php
namespace uat;
use \WebGuy;

class MWSD1262Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckGATrackerDYO(WebGuy $I) {
        $I->wantTo('Check GA tracker for DYO Products');
        $I->expectTo('See GA script on the DYO frame');
        $I->amOnPage('custom-hard-hat-safety-labels-on-a-roll-ff0638-ca.html');
        $I->canSee("_gaq.push('_trackEvent', 'DYO', 'Init', 'custom-hard-hat-safety-labels-on-a-roll-ff0638-ca.html')");
    }

}