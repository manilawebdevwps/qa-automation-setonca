<?php
namespace uat;
use \WebGuy;

class MWSD2597Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function Checkpricelowas(WebGuy $I) {
        $I->wantTo('Check Configurable product page with product lowest price');
        $I->expectTo('See As Low As Price');
        $I->amOnPage('labeltac-reflective-label-supply-w3737.html');
        $I->see('/html/body/div[1]/div[6]/div/div[2]/div/div[2]/form/div[3]');
    }

}

