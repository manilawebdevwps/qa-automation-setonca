<?php
$I = new WebGuy($scenario);
$I->wantTo('perform actions and see result');

$I->amOnPage('customer/account/login/');
$I->fillField('#email','jeric_realubit@bradycorp.com');
$I->fillField('#pass','jeric123');
$I->click('#send2');
$I->amOnPage('stop-signs-30-x-30-sc587.html');
$I->click('#btnaddtocart');
sleep(10);
$I->click('img[alt="Cart"]');
$I->click('button[title="BEGIN SECURE CHECKOUT"]');
$I->click('button[title="Continue"]');
sleep(10);
$I->fillField('ship_to_deliverypoint','Test only');
sleep(10);
$I->click('/html/body/div/div[6]/div/div/div/div/ol/li[3]/div[2]/div/div/form/div[3]/button');
sleep(10);
$I->click('/html/body/div/div[6]/div/div/div/div/ol/li[4]/div[2]/form/fieldset/dl/dl/div/div[2]/dt[2]/input');
sleep(10);
$I->click('/html/body/div/div[6]/div/div/div/div/ol/li[4]/div[2]/div[2]/button');
sleep(10);
$I->seeElement('input[type="checkbox"]');
