<?php
namespace uat;
use \WebGuy;
use Codeception\Module;

class MWSD1535Cest extends \Codeception\Module
{

    /* SCA Filters Improvement - Add # of products next to refinement */

    public function _before()
    {
    }

    public function _after()
    {
    }


    public static $resultFilter= '#fme_layered_color_ca > ol:nth-child(1) > li:nth-child(1) > span:nth-child(2)';
    public static $element_blockFilter = '.block-content';
    public static $element_productResult ='.amount';
    public static $product_testColor ='tags/inspection.html';
    public static $product_testOtherFilter='traffic-and-parking-controls/cones-and-barricades/stanchions/indoor-stanchions.html';
    public static $element_colorDropDowm ='dt.fme_layered_dt:nth-child(1)';
    public static $element_sizeDropdown='dt.fme_layered_dt:nth-child(3)';
    public static $element_materialDropDown='dt.fme_layered_dt:nth-child(5)';
    public static $element_firstFilterColor='#color_ca-6700';
    public static $element_firstFilterSize='//*[@id="fme_filters_list"]/dt[2]';
    public static $element_firstFilterMaterial='//*[@id="fme_filters_list"]/dt[3]';

        // tests
    public function testColorAttributes(WebGuy $I) {
        $I->wantTo('Check filters');
        $I->expectTo('see filter column on the last node of taxonomy');
        $I->amOnPage(self::$product_testColor);
        $I->seeElement(self::$element_blockFilter);
        $firstFilter = $I->grabTextFrom(self::$resultFilter);
        $numOfProductFilter = $I->getNumOfProduct($firstFilter);
        $I->expectTo('see number of products on split '.$numOfProductFilter);
        $I->click('.fme_layered_attribute');
        $I->wait(15);
        $messageResult = $I->grabTextFrom(self::$element_productResult);
        $I->expectTo('see number of products on result '.$messageResult);
        $numOfProductResult = trim(substr( $messageResult,0,strrpos($messageResult,'Product')));
        $I->expectTo('see number of products on result '.$numOfProductResult);
        if (trim($numOfProductFilter) == $numOfProductResult ){
            $I->expectTo('see OK');
        }else {
            $I->raiseError('values are not the same');
        }
    }


    public function testOtherFilters(WebGuy $I){
        $I->wantTo('Check other filters - size and materials');
        $I->expectTo('see filter column on the last node of taxonomy');
        $I->amOnPage(self::$product_testOtherFilter);
        $I->seeElement(self::$element_colorDropDowm);
        $I->seeElement(self::$element_sizeDropdown);
        $I->seeElement(self::$element_materialDropDown);
        $I->click(self::$element_firstFilterColor);
        $I->wait(3);
        $I->see('8 Products','.amount');
        //size
        $I->click(self::$element_firstFilterSize);
        $I->wait(3);
        $I->click('#size_ca-11696');
        $I->wait(3);
        $I->see('3 Products','.amount');
        //material
        $I->click(self::$element_firstFilterMaterial);
        $I->wait(3);
        $I->click('#material_ca-6858');
        $I->wait(3);
        $I->see('1 Product','.amount');

    }



}


