<?php
namespace uat;
use \WebGuy;

class MWSD1459Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckBoxasGuestCheckout(WebGuy $I) {
        $I->wantTo('See checkbox for Commercial Emails for Guest Checkout');
        $I->maximizeWindow();
        $I->amOnPage('/pipe-markers-and-valve-tags/valve-tags/brass-valve-tags/brass-tags-single-pb3060.html');
        $I->waitForElementVisible('.product-view',10);
        $I->fillField('super_group[160177]','2');
        $I->click('.btn-add-to-cart-plus');
        //$I->waitForUserInput();
        $I->waitForElementVisible('.popup-btm',5);
        $I->expectTo('See One Page Checkout');
        $I->click('.btn-view-cart-pop');
        $I->waitForElementVisible('.cart',10);
        $I->expectTo('See Billing Information Tab');
        $I->click('.btn-secure-checkout');

        $I->checkOption('#login:guest');
        $I->expectTo('See Billing Information Tab');
        $I->click('.btn-continue');
        $I->fillField('#billing:firstname','Brady');
        $I->fillField('#billing:lastname','Brady');
        $I->fillField('#billing:company','Brady');
        $I->fillField('#billing:email','bradytestqa@gmail.com');
        $I->fillField('#billing:street1','3rd Floor Worldwide Corporate Center');
        $I->fillField('#billing:city','Mandaluyong');
        $I->selectOption('#billing:region_id', 'Saskatchewan');
        $I->fillField('#billing:postcode','1234');
        $I->fillField('#billing:telephone','123-456-7890');
        $I->canSee('Im in! Please sign me up for your eNewsletter and exclusive offers.');
        $I->canSeeElement('//*[@id="billing:use_for_shipping_no"]');

        $I->click('//*[@id="billing-buttons-container"]/button');
        $I->expectTo('See Shipping Method Tab');
        $I->waitForElementVisible('#checkout-step-shipping_method',5);
        $I->click('//*[@id="shipping-method-buttons-container"]/button');
        $I->wait(5);

        $I->expectTo('See Payment Information Tab');
        $I->waitForElementVisible('#opc-payment',5);
        $I->selectOption('#p_method_checkmo','checkmo');
        $I->click('//*[@id="payment-buttons-container"]/button');
        $I->wait(5);

        $I->expectTo('See Order Review Tab');
        $I->waitForElementVisible('#opc-review',5);
        $I->canSee('Im in! Please sign me up for your eNewsletter and exclusive offers.');
        $I->canSeeElement('input[type="checkbox"]');
        $I->checkOption('#confirm_email');
        $I->click('//*[@id="review-buttons-container"]/button');
        $I->waitForElementVisible('//*[@id="content-container"]/div[6]/div/div[1]/div/div[1]/div[2]/h2',10);

        $I->canSeeInCurrentUrl('/checkout/onepage/success/');
        $I->amOnPage('sapsync/order/create');
        $I->waitForUserInput();
        $I->canSee('SET BUYER CONTACT EMAIL PREFERENCE TO "I"','//*[@id="content-container"]/div[6]/div/div[1]/div/table/tbody/tr[2]/td/soap-env:envelope');

    }

}